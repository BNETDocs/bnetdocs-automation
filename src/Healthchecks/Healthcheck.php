<?php

namespace BNETDocs\Automation\Healthchecks;

abstract class Healthcheck {

  protected $label;

  public function __construct( $label ) {
    $this->label = $label;
  }

  abstract public function assess( $param );

  public function getLabel() {
    return $this->label;
  }

}
