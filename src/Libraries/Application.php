<?php

namespace BNETDocs\Automation\Libraries;

use \BNETDocs\Automation\Healthchecks\Cache as CacheHealth;
use \BNETDocs\Automation\Healthchecks\PhoenixServers as PhoenixServersHealth;

use \BNETDocs\Automation\Libraries\CommandProcessor;

use \CarlBennett\MVC\Libraries\Cache;
use \CarlBennett\MVC\Libraries\Common;
use \CarlBennett\MVC\Libraries\DatabaseDriver;
use \CarlBennett\MVC\Libraries\Term;

final class Application {

    public static $healthchecks;

    /**
     * __construct()
     * Constructor made private to prevent object creation
     */
    private function __construct() {}

    /**
     * init()
     * Initializes the application and makes ready for run()
     *
     * @return void
     */
    public static function init() {

        date_default_timezone_set('Etc/UTC');

        Term::stderr( 'reading configuration' . PHP_EOL );

        Common::$config = json_decode( file_get_contents(
            __DIR__ . '/../../etc/config.json'
        ));

        Common::$database = null;

        Common::$cache = new Cache(
            Common::$config->memcache->servers,
            Common::$config->memcache->connect_timeout,
            Common::$config->memcache->tcp_nodelay
        );

        DatabaseDriver::$character_set = Common::$config->mysql->character_set;
        DatabaseDriver::$database_name = Common::$config->mysql->database;
        DatabaseDriver::$password      = Common::$config->mysql->password;
        DatabaseDriver::$servers       = Common::$config->mysql->servers;
        DatabaseDriver::$timeout       = Common::$config->mysql->timeout;
        DatabaseDriver::$username      = Common::$config->mysql->username;

        self::$healthchecks = array(
            new CacheHealth( 'cache' ),
            new PhoenixServersHealth( 'phoenix_servers' ),
        );

        Term::stderr( 'application initialized' . PHP_EOL );

    }

    /**
     * run()
     * Runs the application based on command requested from command-line
     *
     * @param int   $argc the number of arguments in the set
     * @param array $argv the set of arguments passed to the application
     *
     * @return int application exit status
     */
    public static function run( $argc, $argv ) {

        // don't run with scissors, walk.

        Term::stderr( 'application running' . PHP_EOL );

        $executable = array_shift( $argv );

        if ( $argc < 2 ) {
            Term::stderr( 'usage: ' . $executable . ' <command>' . PHP_EOL );
            return 1;
        }

        return ( CommandProcessor::process( $argv ) ? 0 : 1 );
    }

}
