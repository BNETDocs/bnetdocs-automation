<?php
/**
 *  BNETDocs Automation, the background service application for BNETDocs
 *  Copyright (C) 2008-2018  Carl Bennett
 *  This file is part of BNETDocs.
 *
 *  BNETDocs is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BNETDocs is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BNETDocs.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace BNETDocs\Automation;

use \BNETDocs\Automation\Libraries\Application;

function main( $argc, $argv ) {

    if ( php_sapi_name() !== 'cli' ) {
        exit( 'Project must be ran with php-cli.' . PHP_EOL );
    }

    if ( !file_exists( __DIR__ . '/../lib/autoload.php' )) {
        exit( 'Project misconfigured, run `composer install`' . PHP_EOL );
    }
    require( __DIR__ . '/../lib/autoload.php' );

    Application::init();

    return Application::run( $argc, $argv );

}

exit( main( $argc, $argv ));
