<?php

namespace BNETDocs\Automation\Libraries;

use \CarlBennett\MVC\Libraries\Common;
use \CarlBennett\MVC\Libraries\DatabaseDriver;

use \PDO;
use \RuntimeException;

class PhoenixServer {

  const STATUS_OFFLINE  = 0;
  const STATUS_ONLINE   = 1;
  const STATUS_DISABLED = 2;

  public $address;
  public $created_datetime;
  public $id;
  public $label;
  public $port;
  public $status_bitmask;
  public $type_id;
  public $updated_datetime;
  public $user_id;

  public function __construct( $id = null ) {
    if ( is_null( $id )) { return; }

    if ( is_numeric( $id )) {

      if ( !isset( Common::$database )) {
        Common::$database = DatabaseDriver::getDatabaseObject();
      }

      $stmt = Common::$database->prepare('
        SELECT `id`,
               `user_id`,
               `type_id`,
               `created_datetime`,
               `updated_datetime`,
               `status_bitmask`,
               `label`,
               `address`,
               `port`
        FROM `servers` WHERE `id` = :id LIMIT 1;
      ');

      $stmt->bindParam( ':id', $id, PDO::PARAM_INT );

      if ( !$stmt->execute() || $stmt->rowCount() !== 1 ) {
        throw new RuntimeException(
          'database error fetching phoenix server by id'
        );
      }

      $row = $stmt->fetch( PDO::FETCH_OBJ );

    } else {

      $row = $id;

    }

    $this->address          = (string) $row->address;
    $this->created_datetime = (string) $row->created_datetime;
    $this->id               = (int)    $row->id;
    $this->port             = (int)    $row->port;
    $this->status_bitmask   = (int)    $row->status_bitmask;
    $this->type_id          = (int)    $row->type_id;

    $this->label = (
      is_null( $row->label ) ? null :
      (string) $row->label
    );

    $this->updated_datetime = (
      is_null( $row->updated_datetime ) ? null :
      (string) $row->updated_datetime
    );

    $this->user_id = (
      is_null( $row->user_id ) ? null :
      (int) $row->user_id
    );

    if ( isset( $stmt )) { $stmt->closeCursor(); }
  }

  public function update() {
    if ( !isset( Common::$database )) {
      Common::$database = DatabaseDriver::getDatabaseObject();
    }

    $stmt = Common::$database->prepare('
      UPDATE `servers`
      SET `user_id` = :user_id,
          `type_id` = :type_id,
          `created_datetime` = :created_datetime,
          `updated_datetime` = :updated_datetime,
          `status_bitmask` = :status_bitmask,
          `label` = :label,
          `address` = :address,
          `port` = :port
      WHERE `id` = :id LIMIT 1;
    ');

    $stmt->bindParam( ':id', $this->id, PDO::PARAM_INT );
    $stmt->bindParam( ':type_id', $this->type_id, PDO::PARAM_INT );
    $stmt->bindParam( ':address', $this->address, PDO::PARAM_STR );
    $stmt->bindParam( ':port', $this->port, PDO::PARAM_INT );

    $stmt->bindParam(
      ':created_datetime', $this->created_datetime, PDO::PARAM_STR
    );
    $stmt->bindParam(
      ':updated_datetime', $this->updated_datetime, PDO::PARAM_STR
    );
    $stmt->bindParam(
      ':status_bitmask', $this->status_bitmask, PDO::PARAM_INT
    );

    if ( is_null( $this->label )) {
      $stmt->bindParam( ':label', $this->label, PDO::PARAM_NULL );
    } else {
      $stmt->bindParam( ':label', $this->label, PDO::PARAM_STR );
    }

    if ( is_null( $this->user_id )) {
      $stmt->bindParam( ':user_id', $this->user_id, PDO::PARAM_NULL );
    } else {
      $stmt->bindParam( ':user_id', $this->user_id, PDO::PARAM_INT );
    }

    $success = $stmt->execute();

    $stmt->closeCursor();

    return $success;
  }

}
