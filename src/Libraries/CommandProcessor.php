<?php

namespace BNETDocs\Automation\Libraries;

use \BNETDocs\Automation\Libraries\Application;
use \CarlBennett\MVC\Libraries\Term;

class CommandProcessor {

    /**
     * process()
     * Processes the command that the user requested
     *
     * @param array $argv the set of parameters given to the application
     *
     * @return bool whether the command was successful
     */
    public static function process( array &$argv ) {

        $command = array_shift( $argv );

        switch( $command ) {
            case 'check_phoenix_servers': {
                $label = 'phoenix_servers';
                $param = ( isset( $argv[0] ) ? array_shift( $argv ) : null );
            }
            case 'check': {
                if ( !isset( $label )) {
                  $label = ( isset( $argv[0] ) ? array_shift( $argv ) : null );
                }
                if ( !isset( $param )) {
                  $param = ( isset( $argv[0] ) ? array_shift( $argv ) : null );
                }

                if ( !empty( $label )) {
                    foreach( Application::$healthchecks as $healthcheck ) {
                        if ( $healthcheck->getLabel() == $label ) {
                            $result = $healthcheck->assess( $param );

                            Term::stdout( $label . ' is ' . (
                                $result ? 'healthy' : 'unhealthy'
                            ) . PHP_EOL );

                            return $result;
                        }
                    }
                    Term::stdout( 'unknown healthcheck: ' . $label . PHP_EOL );
                }

                Term::stdout( 'available healthchecks: ' );

                foreach( Application::$healthchecks as $healthcheck ) {
                    Term::stdout( $healthcheck->getLabel() . ' ' );
                }

                Term::stdout( PHP_EOL );

                return false;
            }
            case 'help': {
                Term::stdout(
                    'command list' . PHP_EOL . PHP_EOL .
                    'check <service>       - checks the healthiness of ' .
                    '<service>' . PHP_EOL .
                    'check_phoenix_servers - alias to: check phoenix_servers' .
                    PHP_EOL .
                    'help                  - this command list' . PHP_EOL
                );

                return true;
            }
            case 'test': {
                Term::stdout( 'test complete' . PHP_EOL );
                return true;
            }
            default: {
                Term::stderr( 'unknown command: ' . $command . PHP_EOL );

                return false;
            }
        }

    }

}
