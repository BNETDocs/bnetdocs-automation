<?php

namespace BNETDocs\Automation\Healthchecks;

use \BNETDocs\Automation\Healthchecks\Healthcheck;
use \BNETDocs\Automation\Libraries\PhoenixServer;

use \CarlBennett\MVC\Libraries\Common;
use \CarlBennett\MVC\Libraries\DatabaseDriver;
use \CarlBennett\MVC\Libraries\Term;

use \PDO;

class PhoenixServers extends Healthcheck {

  const SOCKET_TIMEOUT = 3;

  public function assess( $param ) {
    Term::stderr( 'assessing phoenix servers health...' . PHP_EOL );

    $servers = $this->getServers();

    foreach( $servers as $server ) {
      Term::stdout( sprintf(
        'checking %s:%d [%s]...',
        $server->address, $server->port, $server->label
      ));

      if ( $server->status_bitmask & PhoenixServer::STATUS_DISABLED ) {
        Term::stdout( 'disabled' . PHP_EOL );
        continue;
      }

      $was_online = ( $server->status_bitmask & PhoenixServer::STATUS_ONLINE );
      $is_online  = ( $this->assessServer( $server ));

      Term::stdout( ( $is_online ? 'online' : 'offline' ) . PHP_EOL );

      if ( $was_online !== $is_online ) {
        if ( $is_online ) {
          $server->status_bitmask = (
            $server->status_bitmask | PhoenixServer::STATUS_ONLINE
          );
        } else {
          $server->status_bitmask = (
            $server->status_bitmask & ~PhoenixServer::STATUS_ONLINE
          );
        }

        $server->updated_datetime = date('Y-m-d H:i:s');
        $server->update();
      }
    }

    return true;
  }

  private function assessServer( PhoenixServer &$server ) {
    $timeout = array( 'sec' => self::SOCKET_TIMEOUT, 'usec' => 0 );

    $socket = socket_create( AF_INET, SOCK_STREAM, SOL_TCP );

    socket_set_option( $socket, SOL_SOCKET, SO_RCVTIMEO, $timeout );
    socket_set_option( $socket, SOL_SOCKET, SO_SNDTIMEO, $timeout );

    $success = @socket_connect( $socket, $server->address, $server->port );

    if ( !$success ) {
      if ( $socket ) { socket_close( $socket ); }

      $socket = socket_create( AF_INET6, SOCK_STREAM, SOL_TCP );

      socket_set_option( $socket, SOL_SOCKET, SO_RCVTIMEO, $timeout );
      socket_set_option( $socket, SOL_SOCKET, SO_SNDTIMEO, $timeout );

      $success = @socket_connect( $socket, $server->address, $server->port );
    }

    if ( $socket ) { socket_close( $socket ); }

    return $success;
  }

  private function getServers() {
    if ( !isset( Common::$database )) {
      Common::$database = DatabaseDriver::getDatabaseObject();
    }

    $stmt = Common::$database->prepare('
      SELECT `id`,
             `user_id`,
             `type_id`,
             `created_datetime`,
             `updated_datetime`,
             `status_bitmask`,
             `label`,
             `address`,
             `port`
      FROM `servers` ORDER BY `id` ASC;
    ');
    $stmt->execute();

    $servers = array();

    while( $row = $stmt->fetch( PDO::FETCH_OBJ )) {
      $servers[] = new PhoenixServer( $row );
    }

    $stmt->closeCursor();

    return $servers;
  }

}
