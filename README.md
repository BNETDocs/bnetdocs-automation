# BNETDocs Automation
## Summary
**bnetdocs-automation** is a background service application designed to
facilitate the background processes of
[bnetdocs-web](https://github.com/BNETDocs/bnetdocs-web).

The primary task of this service is to faciliate the status checking of servers
available at the [Server List](https://bnetdocs.org/servers) page.

## Installation
### Clone this repository
```sh
git clone git@github.com:BNETDocs/bnetdocs-automation.git ~/bnetdocs-automation
```

### Install php command-line
#### CentOS 7.x / Fedora 24
```sh
sudo yum install php-cli
```

\* Use `dnf` instead of `yum` if you have it available.

#### Debian / Ubuntu
```sh
sudo apt-get update && sudo apt-get install php-cli
```

### Satisfy composer
Run `composer install` at the root of the repository.

### Configure BNETDocs Automation
```sh
cp ./etc/config.sample.json ./etc/config.json
```

\* Open `config.json` in your favorite text editor and modify it to your
   liking.

### Run a task
You can get a list of tasks available by executing:
```sh
./bin/bnetdocs-automation tasklist
```

You can run a task by executing:
```sh
./bin/bnetdocs-automation run <task-name>
```

## Copyright Disclaimer
This project is licensed under the GPLv3. A copy of the GNU General Public
License can be found [here](/LICENSE.md).

    BNETDocs Automation, the background service application for BNETDocs
    Copyright (C) 2008-2018  Carl Bennett

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

BNETDocs is a documentation and discussion website for Blizzard Entertainment's
Battle.net&trade; and in-game protocols. You hereby acknowledge that BNETDocs
content is offered as is and without warranty. BNETDocs Automation is background
software that facilitates the BNETDocs website. BNETDocs is not affiliated or
partnered with Blizzard Entertainment in absolutely any way. Battle.net&trade;
is a registered trademark of Blizzard Entertainment.
