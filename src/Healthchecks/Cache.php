<?php

namespace BNETDocs\Automation\Healthchecks;

use \BNETDocs\Automation\Healthchecks\Healthcheck;
use \CarlBennett\MVC\Libraries\Common;

class Cache extends Healthcheck {

    public function assess( $param ) {

        $key   = hash( 'md5', $param . mt_rand() );
        $value = hash( 'md5', $param . mt_rand() );

        Common::$cache->set( $key, $value, 10 );

        $cache_value = Common::$cache->get( $key );

        Common::$cache->delete( $key );

        return ( $value === $cache_value );

    }

}
